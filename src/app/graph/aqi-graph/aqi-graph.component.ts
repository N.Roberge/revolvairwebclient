import {AfterViewInit, ChangeDetectorRef, Component, Input} from '@angular/core';
import { Chart } from 'chart.js';
import {ObjectKeyService} from '../../services/object-key-service/object-key.service';
import {ColorAqiEnum} from '../color-enum';

@Component({
  selector: 'aqi-graph',
  templateUrl: './aqi-graph.component.html',
  styleUrls: ['./aqi-graph.component.css']
})
export class AqiGraphComponent implements AfterViewInit {

  constructor(private cdRef:ChangeDetectorRef) { }

  @Input() data: number[];
  @Input() pollutant: string;
  chart : Chart;
  min = 1000;
  max = -1;
  current : number;

  ngAfterViewInit() {
        let labels = [];
        let colors = [];

        let dataArray = this.setArrays(labels, this.data, colors);

        this.createChart(dataArray, labels, colors);
        this.cdRef.detectChanges();
  }

  private createChart(array, labels, colors){
    let chartId = "canvas" + this.pollutant;
    let data = {
      labels: labels,
      datasets: [
        {
          data: array,
          borderColor: colors,
          backgroundColor: colors,
          fill: false
        }
      ]
    };
    let options = {
      responsive:true,
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          display: true
        }],
        yAxes: [{
          display: true,
          ticks:{
            min: 0
          }
        }],
      }
    };
    this.chart = new Chart(chartId, {
      type: 'bar',
      data: data,
      options: options
    });
  }

  private setArrays(labels, data, colors){
    let dataArray = [];
    this.current = data[1];
    for(let count = ObjectKeyService.getObjectKeys(data).length; count > 0; count--){
      dataArray[ObjectKeyService.getObjectKeys(data).length - count] = data[count];
      if(data[count] > this.max){
        this.max = data[count];
      }
      if(data[count] < this.min){
        this.min = data[count]
      }
    }

    for(let count = dataArray.length - 1; count >= 0; count--){
      if(dataArray[count] <= ColorAqiEnum.max_green)
        colors[count] = ColorAqiEnum.green;
      if(dataArray[count] > ColorAqiEnum.max_green && dataArray[count] <= ColorAqiEnum.max_yellow)
        colors[count] = ColorAqiEnum.yellow;
      if(dataArray[count] > ColorAqiEnum.max_yellow && dataArray[count] <= ColorAqiEnum.max_orange)
        colors[count] = ColorAqiEnum.orange;
      if(dataArray[count] > ColorAqiEnum.max_orange && dataArray[count] <= ColorAqiEnum.max_red)
        colors[count] = ColorAqiEnum.red;
      if(dataArray[count] > ColorAqiEnum.max_red && dataArray[count] <= ColorAqiEnum.max_purple)
        colors[count] = ColorAqiEnum.purple;
      if(dataArray[count] > ColorAqiEnum.max_purple)
        colors[count] = ColorAqiEnum.crimson;
    }

    for(let count = dataArray.length - 1; count >= 0; count--){
      labels.push(count);
    }

    return dataArray;
  }

}
