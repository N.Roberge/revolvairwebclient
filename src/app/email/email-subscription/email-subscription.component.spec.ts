import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { EmailSubscriptionComponent } from './email-subscription.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EmailService} from '../../services/email-service/email.service';
import {FormsModule} from '@angular/forms';
import {Observable} from 'rxjs/Observable';

describe('EmailSubscriptionComponent', () => {
  let component: EmailSubscriptionComponent;
  let fixture: ComponentFixture<EmailSubscriptionComponent>;
  let emailService: EmailService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        EmailSubscriptionComponent
      ],
      providers: [
        EmailService,
      ],
      imports: [
        HttpClientTestingModule,
        FormsModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(EmailSubscriptionComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    emailService = TestBed.get(EmailService);
  }));

  afterEach(async(() => {
    component = null;
    fixture.destroy();
    emailService = null;
  }));

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create emailService', () => {
    expect(emailService).toBeTruthy();
  });

  describe('onSubmit()', () => {
    it('should call emailService subscribe method once', fakeAsync(() => {
      //Arrange
      spyOn(emailService, 'subscribe').and.returnValue(Observable.of([]));
      tick();

      //Act
      component.onSubmit();
      tick();

      //Assert
      expect(emailService.subscribe).toHaveBeenCalledTimes(1);
    }));
  });

  describe('onSubmit()', () => {
    it('should set submitted value to true if response successful', fakeAsync(() => {
      //Arrange
      spyOn(emailService, 'subscribe').and.returnValue(Observable.of([]));
      tick();

      //Act
      component.onSubmit();
      tick();

      //Assert
      expect(component.submitted).toBe(true);
    }));
  });
});
