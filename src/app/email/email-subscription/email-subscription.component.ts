import { Component, OnInit } from '@angular/core';

import { Mail } from '../../models/mail';
import { EmailService } from '../../services/email-service/email.service';

@Component({
  selector: 'app-email-subscription',
  templateUrl: './email-subscription.component.html',
  styleUrls: ['./email-subscription.component.css']
})
export class EmailSubscriptionComponent implements OnInit {

  constructor(private emailService: EmailService) { }

  model = new Mail('', '');
  submitted = false;
  errors;

  onSubmit() {
    this.emailService.subscribe(this.model.email, this.model.name).subscribe(
      result => console.log(result),
      error => this.errors = error.error.errors[Object.keys(error.error.errors)[0]][0],
      () => this.submitted = true);
  }

  ngOnInit() { }
}
