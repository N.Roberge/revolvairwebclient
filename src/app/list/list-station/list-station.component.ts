import { Component, OnInit } from '@angular/core';
import {Station} from '../../models/station';
import {StationService} from '../../services/station-service/station.service';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {ObjectKeyService} from '../../services/object-key-service/object-key.service';

@Component({
  selector: 'list-station',
  templateUrl: './list-station.component.html',
  styleUrls: ['./list-station.component.css'],
})
export class ListStationComponent implements OnInit {

  constructor(
    private stationService: StationService,
    private router: Router
    ) { }

  observable: Observable<Station[]>;
  currentPage: number;
  private itemsPerPage = 6;
  private getObjectKeys = ObjectKeyService.getObjectKeys;

  ngOnInit() {
    this.observable = this.stationService.getStations();
  }

  pageChanged(page: number) {
    this.currentPage = page;
  }

  details(id: number){
    this.router.navigate(['detail-station', id]);
  }

}
