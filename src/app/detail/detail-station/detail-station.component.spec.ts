import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { DetailStationComponent } from './detail-station.component';
import {Observable} from 'rxjs/Observable';
import {StationService} from '../../services/station-service/station.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Station} from '../../models/station';
import {NgxPaginationModule} from 'ngx-pagination';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DetailStationComponent', () => {
  let component: DetailStationComponent;
  const stationId = 900;
  let fixture: ComponentFixture<DetailStationComponent>;
  let stationService : StationService;
  let station : Station;

  beforeEach(async(() => {
    station = new Station();
    station.id = stationId;

    TestBed.configureTestingModule({
      declarations: [
        DetailStationComponent
      ],
      providers: [
        StationService,
      ],
      imports: [
        NgxPaginationModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(DetailStationComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    stationService = TestBed.get(StationService);
  }));

  afterEach(async(() => {
    component = null;
    fixture.destroy();
    stationService = null;
    station = null;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should get station from stationService', fakeAsync(() => {
      //Arrange
      spyOn(stationService, 'getStation').and.returnValue(Observable.of([station]));
      tick();

      //Act
      component.ngOnInit();
      tick();

      //Assert
      expect(stationService.getStation).toHaveBeenCalled();
    }));

    it('should get latestAqi from stationService', fakeAsync(() => {
      //Arrange
      spyOn(stationService, 'getLatestAqi').and.returnValue(Observable.of([]));
      tick();

      //Act
      component.ngOnInit();
      tick();

      //Assert
      expect(stationService.getLatestAqi).toHaveBeenCalled();
    }))
  });

  describe('details(id: number)', () => {
    it('should go to details page', fakeAsync(() => {
      //Arrange
      let expected = ["list-station"];
      let spy = spyOn((<any>component).router, 'navigate');
      tick();

      //Act
      component.back();
      tick();

      //Assert
      expect(spy).toHaveBeenCalledWith(expected);
    }))
  });
});
