import {TestBed, inject, async} from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    const store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
    };

    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);

    TestBed.configureTestingModule({
      providers: [AuthService]
    });

    service = TestBed.get(AuthService);
  });

  it('should create the service',
    () => {
      expect(service).toBeTruthy();
    });

  describe('setToken', () => {
    it('should store the token in localStorage',
      () => {
        service.storeToken('sometoken');
        expect(localStorage.getItem('token')).toEqual('sometoken');
      });
  });

  describe('getToken', () => {
    it('should return stored token from localStorage',
      () => {
        localStorage.setItem('token', 'anothertoken');
        expect(service.getToken()).toEqual('anothertoken');
      });
  });
});
