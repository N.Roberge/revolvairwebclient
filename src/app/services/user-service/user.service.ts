import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError} from 'rxjs/operators';
import 'rxjs/add/observable/of';
import {Station} from '../../models/station';
import {environment} from '../../../environments/environment';

export class TokenValidity
{
    valid : boolean;
}

export class Result
{
    result?: string;
    error?: string;
}

@Injectable()
export class UserService {

    private apiUrl = environment.apiUrl;

    constructor(private http: HttpClient) { }

    forgotPassword(email : string): Observable<Object> {
        const url = `${this.apiUrl}/forgotPassword`;

        return this.http.post(url, {
            email: email
        }, {observe: 'body'});
    }

    validateResetToken(token : string): Observable<TokenValidity> {
        const url = `${this.apiUrl}/validateResetToken`;

        return this.http.post<TokenValidity>(url, {token: token}, {observe: 'body'});
    }

    resetPassword(password:string, token:string): Observable<Result>{
        const url = `${this.apiUrl}/resetPassword`;

        return this.http.post<Result>(url, {password: password, token: token});
    }

    private handleError<T> (result?: T) {
        return (): Observable<T> => {
            return Observable.of(result as T);
        };
    }
}
