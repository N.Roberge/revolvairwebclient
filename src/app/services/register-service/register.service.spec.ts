import {TestBed, inject, async} from '@angular/core/testing';

import { RegisterService } from './register.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('RegisterService', () => {
  let registerService: RegisterService;
  const apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [RegisterService]
    });
    registerService = TestBed.get(RegisterService);
  });

  it('should be created', inject([RegisterService], (service: RegisterService) => {
    expect(service).toBeTruthy();
  }));

  describe('register()', () => {
    it('should post new user to apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          const expectedUrl = apiUrl + '/register';
          const email = 'ced.toup@hotmail.com';
          const name = 'Cédric Toupin';
          const password = 'secret';

          //Act
          registerService.register(email, name, password).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'POST'
          });
        })));
  });
});
