import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  private gotoMap(){
      this.router.navigate([""]);
  }

  private gotoSignIn(){
      this.router.navigate(["sign-in"]);
  }

  private gotoSignUp(){
      this.router.navigate(["sign-up"]);
  }

  private gotoSubscribe(){
    this.router.navigate(["subscribe"]);
  }
}
