import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import {LoginService} from '../services/login-service/login.service';
import {AuthService} from '../services/auth-service/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private loginService: LoginService, private authService: AuthService, private router: Router) { }
  model = new User('', '');
  errors;

  ngOnInit() {
  }

  onSubmit() {
    this.loginService.login(this.model.email, this.model.password).subscribe(
      result => this.authService.storeToken(result['token']),
      () => this.errors = 'Le courriel ou le mot de passe inscrit est incorrecte. Réinitialiser votre mot de passe si vous l\'avez oublié.',
      () => this.router.navigate(['list-station']));
  }

    gotoForgotPassword(){
        this.router.navigate(['forgot-password']);
    }
}
